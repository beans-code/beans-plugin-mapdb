package net.beanscode.plugin.mapdb;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;

import org.junit.Test;

import net.beanscode.model.connectors.CassandraConnector;
import net.beanscode.model.connectors.PlainConnector;
import net.beanscode.model.dataset.Dataset;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.tests.BeansTestCase;
import net.beanscode.model.unittests.connectors.ConnectorTestCase;
import net.beanscode.model.unittests.connectors.PlainConnectorUnitTest;
import net.beanscode.plugin.mapdb.MapDbConnector;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.weblibs.user.User;

public class MapDbConnectorUnitTest extends ConnectorTestCase {

	@Test
	public void testReadWriteCassandraConnector() throws IOException, ValidationException {
		FileExt source = new FileExt("src/main/resources/testdata/line-1-10.json");
		
		User user = createTestUser("test-user");
		
		Dataset dataset = new Dataset(user.getUserId(), "test-dataset");
		dataset.save();
		
		Table table = new Table(dataset, "test-table");
		table.save();
		
		runAllJobs();
		
		LibsLogger.debug(PlainConnectorUnitTest.class, "Writing ", source.getAbsolutePath(), " into Cassandra");
		
		MapDbConnector writer = new MapDbConnector(table.getId());
		for (Row row : new PlainConnector(source)) {
			LibsLogger.debug(MapDbConnectorUnitTest.class, "Row read from PlainConnector: ", row);
			writer.write(row);
		}
		writer.close();
		
		for (Row row : new MapDbConnector(table.getId())) {
			LibsLogger.debug(MapDbConnectorUnitTest.class, "Row read from MapDbConnector: ", row);
		}
		
		final double sum1Plain = sumColumn(new PlainConnector(source), "x");
		final double sum1MapDb = sumColumn(new MapDbConnector(table.getId()), "x");
		assertTrue(sum1Plain == sum1MapDb, "Sum does not match");
		
		LibsLogger.debug(MapDbConnectorUnitTest.class, "Finished!");
	}
}
