package net.beanscode.plugin.mapdb;

import static java.lang.String.valueOf;
import static net.hypki.libs5.utils.string.StringUtilities.notEmpty;
import static net.hypki.libs5.utils.utils.NumberUtils.toInt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

import net.beanscode.model.cass.Data;
import net.beanscode.model.dataset.Table;
import net.beanscode.model.dataset.TableFactory;
import net.beanscode.model.plugins.ColumnDef;
import net.beanscode.model.plugins.ColumnDefList;
import net.beanscode.model.plugins.Connector;
import net.beanscode.model.plugins.ConnectorInitParams;
import net.hypki.libs5.db.db.DbObject;
import net.hypki.libs5.db.db.Results;
import net.hypki.libs5.db.db.Row;
import net.hypki.libs5.db.db.schema.ColumnType;
import net.hypki.libs5.db.db.schema.TableSchema;
import net.hypki.libs5.db.db.weblibs.C3Clause;
import net.hypki.libs5.db.db.weblibs.C3Where;
import net.hypki.libs5.db.db.weblibs.ClauseType;
import net.hypki.libs5.db.db.weblibs.Mutation;
import net.hypki.libs5.db.db.weblibs.MutationType;
import net.hypki.libs5.db.db.weblibs.ValidationException;
import net.hypki.libs5.db.db.weblibs.utils.UUID;
import net.hypki.libs5.db.mapdb.MapDbManager;
import net.hypki.libs5.search.query.Query;
import net.hypki.libs5.utils.LibsLogger;
import net.hypki.libs5.utils.file.FileExt;
import net.hypki.libs5.utils.json.JsonUtils;
import net.hypki.libs5.utils.reflection.SystemUtils;
import net.hypki.libs5.utils.string.StringUtilities;
import net.hypki.libs5.weblibs.WeblibsConst;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Deprecated
public class MapDbConnector implements Connector, Iterable<Row> {
	
	private final static String KEYSPACE = "beansdata";
	
	private final static String COLUMN_FAMILY_COLUMN_DEFS = "columndefs";
	
	private static MapDbManager mapDbProvider = null;
	
	private ConnectorInitParams connectorInitParams = null;
	
	private List<Mutation> toSave = null;

	public MapDbConnector() {
		
	}
	
	public MapDbConnector(UUID tableId) {
		getInitParams().setTableId(tableId);
	}
	
	@Override
	public void clear() {
		getMapDbProvider().clearTable(KEYSPACE, getTable());
	}

	private static MapDbManager getMapDbProvider() {
		if (mapDbProvider == null)
			mapDbProvider = new MapDbManager(new FileExt(SystemUtils.getHomePath(), ".beans/mapdbconnector/mapdbconnector.mapdb").getAbsolutePath()) {
				@Override
				public TableSchema getTableSchema(String keyspace, String table) {
					return new TableSchema(table)
						.addColumn(DbObject.COLUMN_PK, ColumnType.STRING, true)
						.addColumn(DbObject.COLUMN_DATA, ColumnType.STRING);
				}
		};
		return mapDbProvider;
	}

	@Override
	public String getName() {
		return "MapDbConnector";
	}

	@Override
	public ConnectorInitParams getInitParams() {
		if (connectorInitParams == null)
			connectorInitParams = new ConnectorInitParams();
		return connectorInitParams;
	}

	@Override
	public Connector setInitParams(ConnectorInitParams initParams) {
		this.connectorInitParams = initParams;
		return this;
	}
	
	@Override
	public void validate() throws IOException {
		// do nothing
	}

	@Override
	public String getVersion() {
		return getClass().getSimpleName() + ", ver. 1.0";
	}
	
//	@Override
//	public Iterable<Row> iterateRows() {
//		return new Iterable<Row>() {
//			@Override
//			public Iterator<Row> iterator() {
//				return MapDbConnector.this.iterator();
//			}
//		};
//	}
	
	@Override
	public long getAproxBytesSize() throws IOException {
		return 0;
	}
	
	@Override
	public Iterator<Row> iterator() {
		return new Iterator<Row>() {

			Iterator<Row> rowIter = getMapDbProvider().tableIterable(KEYSPACE, getTable()).iterator();
			ColumnDefList columnDefs = getColumnDefs();
			
			@Override
			public Row next() { 
				Row r = rowIter.next();
				JsonObject jo = JsonUtils.parseJson(r.getAsString(DbObject.COLUMN_DATA)).getAsJsonObject();
				r.getColumns().clear();
				r.setPk(jo.get(DbObject.COLUMN_PK));
//				for (JsonElement je : jo.get("columns").getAsJsonArray()) {
//					r.addColumn(je. getAsJsonObject().get, value)
//				}
//				r.setColumns(jo.get("columns").getasjson);
				JsonObject columns = jo.get("columns").getAsJsonObject();
				if (columnDefs != null)
					for (ColumnDef col : columnDefs) {
	//					r.addColumn(col.getName(), columns.get(col.getName()));
						if (col.getType() == ColumnType.DOUBLE)
							r.addColumn(col.getName(), columns.get(col.getName()).getAsDouble());
						else if (col.getType() == ColumnType.INTEGER)
							r.addColumn(col.getName(), columns.get(col.getName()).getAsInt());
						else if (col.getType() == ColumnType.LONG)
							r.addColumn(col.getName(), columns.get(col.getName()).getAsLong());
						else if (col.getType() == ColumnType.STRING)
							r.addColumn(col.getName(), columns.get(col.getName()).getAsString());
						else
							throw new NotImplementedException();
					}
				return r;
				
//				Row tmp = rowIter.next();
//				return JsonUtils.fromJson(tmp.getAsString(DbObject.COLUMN_DATA), Row.class);
			}
			
			@Override
			public boolean hasNext() {
				return rowIter.hasNext();
			}
		};
	}

//	@Override
//	public List<Row> readRows(String... pk) throws IOException {
//		List<Row> rows = new ArrayList<Row>();
//		for (String onePk : pk) {			
//			rows.addAll(getMapDbProvider().getList(KEYSPACE, 
//				getTable(), 
//				new String[]{DbObject.COLUMN_DATA}, 
//				null, 
//				pk.length, 
//				null, 
//				new C3Where()
//					.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, onePk))
//				).getRows());
//		}
//		return rows;
//	}

	@Override
	public void write(Row row) throws IOException {
		try {
			if (toSave == null)
				toSave = new ArrayList<Mutation>();
			
			if (row.getPk() == null)
				row.setPk(UUID.random().getId());
			
			toSave.add(new Mutation(MutationType.INSERT, 
					KEYSPACE, 
					getTable(), 
					row.getPk(), 
					DbObject.COLUMN_PK, 
					DbObject.COLUMN_DATA, 
					row.toJson()));
			
			if (toSave.size() > 500) {
				getMapDbProvider().save(toSave);
				LibsLogger.debug(MapDbConnector.class, "Saved ", toSave.size(), " rows");
				toSave.clear();
			}
		} catch (ValidationException e) {
			throw new IOException("Cannot save rows", e);
		}
	}

	@Override
	public void close() throws IOException {
		try {
			if (toSave != null && toSave.size() > 0) {
				getMapDbProvider().save(toSave);
				LibsLogger.debug(MapDbConnector.class, "Saved ", toSave.size(), " rows");
				toSave.clear();
			}
		} catch (ValidationException e) {
			throw new IOException("Cannot save rows", e);
		}
	}

	@Override
	public void setColumnDefs(ColumnDefList defs) {
		try {
			List<Mutation> mutations = new ArrayList<Mutation>();

			mutations.add(new Mutation(MutationType.INSERT, 
					KEYSPACE, 
					COLUMN_FAMILY_COLUMN_DEFS, 
					getInitParams().getTableId().getId(), 
					DbObject.COLUMN_PK, 
					DbObject.COLUMN_DATA, 
					JsonUtils.objectToStringPretty(defs)));
			
			getMapDbProvider().save(mutations);
		} catch (IOException | ValidationException e) {
			LibsLogger.error(MapDbConnector.class, "Cannot save ColumnDef list for a table " + getInitParams().getTableId(), e);
		}
	}

	@Override
	public ColumnDefList getColumnDefs() {
		try {
			String tmp = getMapDbProvider().get(KEYSPACE, 
					COLUMN_FAMILY_COLUMN_DEFS, 
					new C3Where()
						.addClause(new C3Clause(DbObject.COLUMN_PK, ClauseType.EQ, getInitParams().getTableId().getId())), 
						DbObject.COLUMN_DATA);
			if (StringUtilities.nullOrEmpty(tmp)) {
				Table table = TableFactory.getTable(getInitParams().getTableId());
				return table != null ? table.getColumnDefs() : null;
			}
			
			ColumnDefList defs = new ColumnDefList();
			for (JsonElement je : JsonUtils.parseJson(tmp).getAsJsonArray()) {
				defs.add(JsonUtils.fromJson(je, ColumnDef.class));
			}
			return defs;
		} catch (IOException e) {
			LibsLogger.error(MapDbConnector.class, "Cannot get ColumnDef from DB", e);
			return null;
		}
	}

	private String getTable() {
		return getInitParams().getTableId().getId();
	}

//	@Override
//	public Results iterateRows(String state, int size) {
//		Results cass = getMapDbProvider().getList(KEYSPACE, 
//				//Data.COLUMN_FAMILY,
//				getTable(),
//				new String[] {DbObject.COLUMN_DATA},
//				null,
//				size, 
//				state,
//				null
////				new C3Where()
////					.addClause(new C3Clause(Table.COLUMN_TBID, ClauseType.EQ, getTable()))
//					);
//		
//		Results res = new Results();
//		res.setNextPage(cass.getNextPage());
//		for (Row row : cass) {
//			res.addRow(JsonUtils.fromJson(row.getAsString(DbObject.COLUMN_DATA), Row.class));
//		}
//		return res;
//	}

	@Override
	public Iterable<Row> iterateRows(Query filter, int splitNr, int splitCount) {
		// TODO implement it
		if (splitNr == 0)
			return iterateRows(filter);
		else
			return new Iterable<Row>() {
				
				@Override
				public Iterator<Row> iterator() {
					return new Iterator<Row>() {
						
						@Override
						public Row next() {
							return null;
						}
						
						@Override
						public boolean hasNext() {
							return false;
						}
					};
				}
			};
	}
	
	@Override
	public Iterable<Row> iterateRows(Query filter) {
		return new Iterable<Row>() {
			
			@Override
			public Iterator<Row> iterator() {
				return new Iterator<Row>() {
					
					private Iterator<Row> allRowsIter = iterateRows(filter).iterator();
					private Row row = null;
					
					@Override
					public Row next() {
						return row;
					}
					
					@Override
					public boolean hasNext() {
						while (allRowsIter.hasNext()) {
							row = allRowsIter.next();
							
							if (row == null)
								return false;
							else if (filter.isFulfilled(row))
								return true;
						}
						
						return row != null;
					}
				};
			}
		};
	}
	
	@Override
	public Results iterateRows(Query filter, String state, int size) {
		int from = 0;
		if (notEmpty(state))
			from = toInt(state);
		
		int i = 0;
		Results res = new Results();
		for (Row row : this.iterateRows(filter)) {
			if (i >= from)
				res.addRow(row);
			
			i++;
			
			if (res.size() == size)
				break;
		}
		res.setNextPage(valueOf(i));
		return res;
	}
}
